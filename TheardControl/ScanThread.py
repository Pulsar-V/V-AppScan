# -*- coding: utf-8 -*-
#!usr/bin/python
'''
Created on 2017/8/7.

@author: Summer-V
'''

class Scan():
    def __init__(self):
        print("Load Scan")

    def split_ip(self, a, b):
        a_ip_1 = int(a.split(".")[0])
        a_ip_2 = int(a.split(".")[1])
        a_ip_3 = int(a.split(".")[2])
        a_ip_4 = int(a.split(".")[3])
        b_ip_1 = int(b.split(".")[0])
        b_ip_2 = int(b.split(".")[1])
        b_ip_3 = int(b.split(".")[2])
        b_ip_4 = int(b.split(".")[3])
        dot = "."
        IP = []
        count = -1
        if a_ip_1 > b_ip_1:
            return False
        if a_ip_1 == b_ip_1:
            if a_ip_2 > b_ip_2:
                return False
        if a_ip_1 == b_ip_1 and a_ip_2 == b_ip_2:
            if a_ip_3 > b_ip_3:
                return False
        if a_ip_1 == a_ip_1 and a_ip_2 == b_ip_2 and a_ip_3 == b_ip_3:
            if a_ip_4 > b_ip_4:
                return False
        while 1:
            count += 1
            url_ip = str(a_ip_1) + dot + str(a_ip_2) + dot + str(a_ip_3) + dot + str(a_ip_4)
            IP.append(url_ip)
            if a_ip_1 == b_ip_1 and a_ip_2 == b_ip_2 and a_ip_3 == b_ip_3 and a_ip_4 == b_ip_4:
                print(count)
                return IP
                break
            a_ip_4 = a_ip_4 + 1
            if a_ip_4 == 256:
                a_ip_3 += 1
                a_ip_4 = 1
            if a_ip_3 == 256:
                a_ip_3 = 1
                a_ip_2 += 1
            if a_ip_2 == 256:
                a_ip_2 = 1
                a_ip_1 += 1


if __name__ == "__main__":
    scan = Scan()
    print(scan.split_ip("192.16.2.78", "192.168.1.255"))



